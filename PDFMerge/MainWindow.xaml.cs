﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.Collections.ObjectModel;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace PDFMerge
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<PdfPageListElement> PdfPages;
        public MainWindow()
        {
            InitializeComponent();
            //Initialise List Box
            PdfPages = new List<PdfPageListElement>();
            lbPDFPages.ItemsSource = PdfPages;

        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            //Get PDF and Open it
            string FilePath = txtFilePath.Text;
            if (FilePath != "")
            {
                PdfDocument Input = PdfReader.Open(FilePath, PdfDocumentOpenMode.Import);

                //Get File Name
                String PdfName = System.IO.Path.GetFileNameWithoutExtension(FilePath);

                int Index = 1;

                foreach (PdfPage Page in Input.Pages)
                {
                    //Construct List Element Name
                    string PageName = PdfName + "-" + Index;
                    //Add To List Box Model
                    PdfPages.Add(new PdfPageListElement(PageName, Page));

                    Index++;
                }
                //Refresh List Box
                lbPDFPages.Items.Refresh();

                setButtons(true);
            }
        }

        private void btnSelectFile_Click(object sender, RoutedEventArgs e)
        {
            //Create A File Dialog for PDFs
            Microsoft.Win32.OpenFileDialog FileDialog = new Microsoft.Win32.OpenFileDialog();
            FileDialog.Filter = "PDF Files (.pdf)|*.pdf";

            bool? Result = FileDialog.ShowDialog();
            //Check that a file has been selected and populate the textbox
            if (Result == true)
            {
                txtFilePath.Text = FileDialog.FileName;
            }
        }

        private void setButtons(Boolean Enable)
        {
            //Enables or disables the buttons in the UI
            btnMerge.IsEnabled = Enable;
            btnRemove.IsEnabled = Enable;
            btnSplit.IsEnabled = Enable;
            btnPageDown.IsEnabled = Enable;
            btnPageUp.IsEnabled = Enable;
        }

        private static void moveListElementUp<T>(ref List<T> list, int index)
        {
            T listElement = list[index];
            list.RemoveAt(index);
            list.Insert(index - 1, listElement);
        }

        private static void moveListElementDown<T>(ref List<T> list, int index)
        {
            T listElement = list[index];
            list.RemoveAt(index);
            list.Insert(index + 1, listElement);
        }

        private void btnPageUp_Click(object sender, RoutedEventArgs e)
        {
            //Move the selected page up in the list
            if (lbPDFPages.SelectedIndex > 0)
            {
                moveListElementUp(ref PdfPages, lbPDFPages.SelectedIndex);
            }
            //Refresh List Box
            lbPDFPages.Items.Refresh();
        }

        private void btnPageDown_Click(object sender, RoutedEventArgs e)
        {
            //Move the selected page down in the list
            if (lbPDFPages.SelectedIndex >= 0 && lbPDFPages.Items.Count > 1)
            {
                moveListElementDown(ref PdfPages, lbPDFPages.SelectedIndex);
            }
            //Refresh List Box
            lbPDFPages.Items.Refresh();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            //Remove the selected page from the list
            if (lbPDFPages.SelectedIndex >= 0 && lbPDFPages.Items.Count >= 1)
            {
                PdfPages.RemoveAt(lbPDFPages.SelectedIndex);
            }
            //Refresh List Box
            lbPDFPages.Items.Refresh();

            if (lbPDFPages.Items.Count < 0)
            {
                setButtons(false);
            }
        }

        private void btnMerge_Click(object sender, RoutedEventArgs e)
        {
            //Create a new PDF Document
            PdfDocument mergedDocument = new PdfDocument();

            //Add all of the pages
            foreach(PdfPageListElement PdfPageListElement in PdfPages)
            {
                mergedDocument.AddPage(PdfPageListElement.PdfPage);
            }

            //Save the merged pdf
            Microsoft.Win32.SaveFileDialog SaveDialog = new Microsoft.Win32.SaveFileDialog();
            SaveDialog.AddExtension = true;
            SaveDialog.DefaultExt = "PDF Files (.pdf)|*.pdf";
            SaveDialog.OverwritePrompt = true;

            bool? result = SaveDialog.ShowDialog();

            if (result == true)
            {
                string fileName = SaveDialog.FileName;
                mergedDocument.Save(fileName);
            }
        }

        private void btnSplit_Click(object sender, RoutedEventArgs e)
        {
            //Create a Folder Dialog
            CommonOpenFileDialog folderDialog = new CommonOpenFileDialog();
            folderDialog.IsFolderPicker = true;

            CommonFileDialogResult? result = folderDialog.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                string folder = folderDialog.FileName;
                //Save each page as it's own PDF
                foreach (PdfPageListElement pdfPageListElement in PdfPages)
                {
                    PdfDocument document = new PdfDocument();
                    document.AddPage(pdfPageListElement.PdfPage);
                    document.Save(folder + "\\" + pdfPageListElement.Name + ".pdf");
                }
                
            }
        }
    }

    public class PdfPageListElement
    {
        public String Name { get; set; }
        public PdfPage PdfPage { get; set; }

        public PdfPageListElement(String name, PdfPage pdfPage)
        {
            this.Name = name;
            this.PdfPage = pdfPage;
        }
    }
}
