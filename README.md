# README #

### What is this repository for? ###

This is a quick and simple utility written in WPF to merge and split PDF Files

### How do I get set up? ###

Just pull down from Git and you should be ready to go.

## Dependencies ##
 - PDFSharp-WPF (https://www.nuget.org/packages/PDFsharp-wpf/1.50.4000-beta3b)
 - WindowsAPICodePack.Shell (https://www.nuget.org/packages/Microsoft.WindowsAPICodePack.Shell/)


### Licence ###
This software is released under the Apache License 2.0. See here for more information: [Apache Licence 2.0](https://www.apache.org/licenses/LICENSE-2.0)